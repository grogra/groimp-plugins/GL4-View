# Changelog

## 0.2.0

- fix crash when disposing renderer
- fix crash when modifying nurbs surface parameters
- respect visibleSides attribute, allow drawing back sides
- tools are no longer shaded
- add changelog
- improve documentation
- fix cube mesh

## 0.1.0

- support for boxes, cones, cylinders, frustums, spheres, lines, points
- lighting and up to 512 light sources
- "render as wireframe"
- linesegmentizable and polygonizable objects
- render tools
- default light source
