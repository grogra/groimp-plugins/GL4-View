package me.mlata.gl4;

import javax.vecmath.Color3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Tuple3f;

import com.jogamp.opengl.GL4;

import me.mlata.gl4.volumes.Lines;
import me.mlata.gl4.volumes.Volume;

/**
 * This class represents a collection of objects with the same {@link Volume}. 
 */
public class ObjectCollection {
    /**
     * Transforms of contained objects.
     */
    ChunkedBuffer transforms;
    /**
     * Colors of contained objects.
     */
    ChunkedBuffer colors;
    
    /**
     * Volume of contained objects.
     */
    Volume volume;

    /**
     * Uniform block binding for transform matrices.
     */
    private static final int MODEL_UNIFORM_BINDING = 0;
    /**
     * Uniform block binding for color data.
     */
    private static final int COLOR_UNIFORM_BINDING = 1;

    /**
     * Max number of instances supported. This has to be the same number as in the used vertex shader.
     * 256 * 16 floats is the maximum possible buffer size guaranteed by the OpenGL specification.
     */
    public static final int CHUNK_INSTANCES = 256;
    public static final int TRANSFORM_CHUNK_SIZE = CHUNK_INSTANCES * 16;
    public static final int COLOR_CNUNK_SIZE = CHUNK_INSTANCES * 4;

    /**
     * Uniform location to enable/disable shading.
     */
    static final int SHADING_UNIFORM_LOCATION = 4;

    /**
     * Creates a new ObjectCollection with the given Volume.
     * @param volume A Volume.
     */
    public ObjectCollection(Volume volume) {
        this.transforms = new ChunkedBuffer(TRANSFORM_CHUNK_SIZE);
        this.colors = new ChunkedBuffer(COLOR_CNUNK_SIZE);
        this.volume = volume;
    }

    /**
     * Binds the next buffer of the ChunkedBuffer containing the objects.
     * @param gl The current OpenGL context.
     * @param target The OpenGL buffer target.
     */
    protected boolean bindNext(GL4 gl, BufferTarget target) {
        this.transforms.bindNext(gl, target);
        return this.colors.bindNext(gl, target);
    }

    /**
     * Binds the next buffer of the ChunkedBuffer containing the objects to the given index.
     * @param gl The current OpenGL context.
     * @param target The OpenGL buffer target.
     * @param index The index to bind to.
     */
    protected boolean bindNext(GL4 gl, BufferTarget target, int index) {
        this.transforms.bindNext(gl, target, index);
        return this.colors.bindNext(gl, target, index);
    }

    /**
     * Resets the next buffer to bind to the first buffer.
     * @param gl The current OpenGL context.
     */
    protected void resetBound(GL4 gl) {
        this.transforms.resetBound(gl);
        this.colors.resetBound(gl);
    }

    /**
     * Draws the objects in this collection using a few instanced draw calls.
     * @param gl The current OpenGL context
     * @param vao The vertex array object to use for rendering
     */
    public void draw(GL4 gl) {
        this.draw(gl, false, false);
    }

    /**
     * Draws the objects in this collection using a few instanced draw calls.
     * @param gl The current OpenGL context
     * @param forceWireframe Whether to render all objects in wireframe mode
     * @param disableShading Whether to disable light calculations
     * @param vao The vertex array object to use for rendering
     */
    public void draw(GL4 gl, boolean forceWireframe, boolean disableShading) {
        // get vao from volume
        VertexArrayObject vao = this.volume.getMesh(gl);

        gl.glUniform1i(SHADING_UNIFORM_LOCATION, vao.shading && !disableShading ? 1 : 0);

        // set wireframe mode
        if (vao.wireframe || forceWireframe) {
            // disable back face culling
            gl.glDisable(GL4.GL_CULL_FACE);
            // set polygon mode to line
            gl.glPolygonMode(GL4.GL_FRONT_AND_BACK, GL4.GL_LINE);
        } else {
            // enable back face culling
            gl.glEnable(GL4.GL_CULL_FACE);
            // set polygon mode to fill
            gl.glPolygonMode(GL4.GL_FRONT_AND_BACK, GL4.GL_FILL);
        }

        // bind vertex arrary object
        vao.bind(gl);

        this.transforms.resetBound(gl);
        this.colors.resetBound(gl);

        // go over all chunks and draw them
        while (this.transforms.bindNext(gl, BufferTarget.UNIFORM, MODEL_UNIFORM_BINDING)) {
            this.colors.bindNext(gl, BufferTarget.UNIFORM, COLOR_UNIFORM_BINDING);

            // check if vao has indices and use appropriate draw call
            if (vao.hasIndices()) {
                // issue draw call
            		gl.glDrawElementsInstanced(
                    vao.getGLPrimitive(),
                    vao.getIndicesCount(),
                    GL4.GL_UNSIGNED_INT,
                    0,
                    this.getCount()
                );
            } else {
                // issue draw call
                gl.glDrawArraysInstanced(
                    vao.getGLPrimitive(), 
                    0,
                    vao.getVerticesCount(),
                    this.getCount()
                );
            }
        }

        // check for OpenGL errors
        int error = gl.glGetError();
        if (error != 0) {
            System.err.println("An OpenGL error occurred: " + error);
        }
    }

    /**
     * @return The number of objects in this collection.
     */
    public int getCount() {
        return this.transforms.getTotalSize() / 16;
    }

    /**
     * @return The Volume of the objects.
     */
    public Volume getVolume() {
        return this.volume;
    }

    /**
     * Adds an object to the collection.
     * This may create a new OpenGL buffer if neccessary.
     * @param gl The current OpenGL context.
     * @param t The transform matrix of the object.
     * @param color The color of the object.
     */
    public void put(GL4 gl, Matrix4f t, Tuple3f color) {
        this.transforms.put(t);
        this.colors.put(new float[] {
            color.x, color.y, color.z, 1
        });
    }

    /**
     * Send all buffers to the GPU.
     */
    public void send(GL4 gl) {
        this.transforms.send(gl);
        this.colors.send(gl);
    }

    /**
     * Clear the collection of all objects.
     */
    public void clear() {
        this.transforms.clear();
        this.colors.clear();
    }

    /**
     * Delete all OpenGL buffers and free their memory.
     */
    protected void delete(GL4 gl) {
        this.transforms.delete(gl);
        this.colors.delete(gl);
        this.volume.deleteVAO(gl);
    }
}
