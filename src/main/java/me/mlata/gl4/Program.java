package me.mlata.gl4;

import java.io.IOException;
import java.nio.file.Path;

import com.jogamp.opengl.GL4;

/**
 * An OpenGL program consisting of a vertex and a fragment shader.
 */
public class Program {
    int id;
    Shader vertex;
    Shader fragment;

    public Program(GL4 gl, Shader vertexShader, Shader fragmentShader) {
        this.id = gl.glCreateProgram();
        this.vertex = vertexShader;
        this.fragment = fragmentShader;

        gl.glAttachShader(this.id, this.vertex.id);
        gl.glAttachShader(this.id, this.fragment.id);
        gl.glLinkProgram(this.id);

        int[] success = new int[] {1};
        gl.glGetProgramiv(this.id, GL4.GL_LINK_STATUS, success, 0);
        if (success[0] == 0) {
            int[] len = new int[] {0};
            gl.glGetProgramiv(this.id, GL4.GL_INFO_LOG_LENGTH, len, 0);
            byte[] infoLogBytes = new byte[len[0]];
            gl.glGetProgramInfoLog(this.id, len[0], len, 0, infoLogBytes, 0);
            String infoLog = new String(infoLogBytes);
            throw new RuntimeException("Program linking failed: " + infoLog);
        }

        gl.glDetachShader(this.id, this.vertex.id);
        gl.glDetachShader(this.id, this.fragment.id);
    }

    public Program(GL4 gl, String vertexShaderSrc, String fragmentShaderSrc) {
        this(
            gl,
            new Shader(gl, vertexShaderSrc, ShaderKind.Vertex),
            new Shader(gl, fragmentShaderSrc, ShaderKind.Fragment)
        );
    }

    public Program(GL4 gl, Path vertexShaderSrc, Path fragmentShaderSrc) throws IOException {
        this(
            gl,
            new Shader(gl, vertexShaderSrc, ShaderKind.Vertex),
            new Shader(gl, fragmentShaderSrc, ShaderKind.Fragment)
        );
    }
    
    public Program(GL4 gl, byte[] vertexShaderSrc, byte[] fragmentShaderSrc) throws IOException {
        this(
            gl,
            new Shader(gl, new String(vertexShaderSrc), ShaderKind.Vertex),
            new Shader(gl, new String(fragmentShaderSrc), ShaderKind.Fragment)
        );
    }

    public void use(GL4 gl) {
        gl.glUseProgram(this.id);
    }

    public void delete(GL4 gl) {
        this.vertex.delete(gl);
        this.fragment.delete(gl);
        gl.glDeleteProgram(this.id);
    }
}
