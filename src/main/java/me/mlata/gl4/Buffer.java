package me.mlata.gl4;

import java.nio.IntBuffer;

import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Tuple3f;
import javax.vecmath.Tuple4f;

import com.jogamp.opengl.GL4;

// TODO: understand and use buffer api
/**
 * A buffer backed by both an array and an OpenGL buffer. Data written to the
 * buffer is not immediately transfered to the GPU, but has to be sent explicitly
 * using {@link send}. The data is stored as integers, but both floats and ints can
 * be written to the buffer to support mixed structs defined in shaders.
 */
public class Buffer {
    int id = -1;
    BufferUsage usage;

    int[] data;
    IntBuffer wrapper;
    int length = 0;

    public static final int MAX_UNIFORM_BUFFER_SIZE = 4096;

    public Buffer(int size, BufferUsage usage) {
        this.usage = usage;

        // create local data array and wrapping buffer
        this.data = new int[size];
        this.wrapper = IntBuffer.wrap(this.data);
    }

    public Buffer(int[] data, BufferUsage usage) {
        this(data.length, usage);
        this.put(data);
    }

    public Buffer(float[] data, BufferUsage usage) {
        this(data.length, usage);
        this.put(data);
    }

    private void createGLBufferIfNeeded(GL4 gl) {
        if (this.id == -1) {
            // create OpenGL buffer and store id
            int[] id = new int[1];
            gl.glCreateBuffers(1, id, 0);
            this.id = id[0];
        }
    }

    /**
     * Returns the underlying array. Changes to the array will change the data
     * in the buffer as long as the buffer is not resized.
     */
    public int[] getArray() {
        return this.data;
    }

    /**
     * Returns amount of elements in the buffer.
     */
    public int length() {
        return this.length;
    }

    public int remaining() {
        return this.wrapper.remaining();
    }

    /**
     * Transfers all data to a new buffer of the specified size.
     */
    public void resize(int size) {
        int[] old = this.data;
        this.data = new int[size];
        for (int i = 0; i < Math.min(size, old.length); i++) {
            this.data[i] = old[i];
        }
        IntBuffer oldBuf = this.wrapper;
        this.wrapper = IntBuffer.wrap(this.data);
        if (oldBuf.position() < size) {
            this.wrapper.position(oldBuf.position());
        } else {
            this.wrapper.position(size);
        }
    }

    public void put(int value) {
        this.wrapper.put(value);
        this.length++;
    }

    public void put(int index, int value) {
        this.wrapper.put(index, value);
    }

    public void put(int[] values) {
        this.wrapper.put(values);
        this.length += values.length;
    }

    public void put(float value) {
        this.wrapper.put(Float.floatToRawIntBits(value));
        this.length++;
    }

    public void put(int index, float value) {
        this.wrapper.put(index, Float.floatToRawIntBits(value));
    }

    public void put(float[] values) {
        for(float value : values) {
            this.wrapper.put(Float.floatToRawIntBits(value));
        }
        this.length += values.length;
    }

    public void put(Tuple3f value) {
        this.put(value.x);
        this.put(value.y);
        this.put(value.z);
    }

    public void put(int index, Tuple3f value) {
        this.put(index, value.x);
        this.put(index, value.y);
        this.put(index, value.z);
    }

    public void put(Tuple4f value) {
        this.put(value.x);
        this.put(value.y);
        this.put(value.z);
        this.put(value.w);
    }

    public void put(int index, Tuple4f value) {
        this.put(index, value.x);
        this.put(index, value.y);
        this.put(index, value.z);
        this.put(index, value.w);
    }

    public void put(Matrix3f value) {
        this.put(value.m00);
        this.put(value.m10);
        this.put(value.m20);
        this.put(value.m01);
        this.put(value.m11);
        this.put(value.m21);
        this.put(value.m02);
        this.put(value.m12);
        this.put(value.m22);
    }

    public void put(int index, Matrix3f value) {
        this.put(index + 0, value.m00);
        this.put(index + 1, value.m10);
        this.put(index + 2, value.m20);
        this.put(index + 3, value.m01);
        this.put(index + 4, value.m11);
        this.put(index + 5, value.m21);
        this.put(index + 6, value.m02);
        this.put(index + 7, value.m12);
        this.put(index + 8, value.m22);
    }

    public void put(Matrix4f value) {
        this.put(value.m00);
        this.put(value.m10);
        this.put(value.m20);
        this.put(value.m30);
        this.put(value.m01);
        this.put(value.m11);
        this.put(value.m21);
        this.put(value.m31);
        this.put(value.m02);
        this.put(value.m12);
        this.put(value.m22);
        this.put(value.m32);
        this.put(value.m03);
        this.put(value.m13);
        this.put(value.m23);
        this.put(value.m33);
    }

    public void put(int index, Matrix4f value) {
        this.put(index + 0, value.m00);
        this.put(index + 1, value.m10);
        this.put(index + 2, value.m20);
        this.put(index + 3, value.m30);
        this.put(index + 4, value.m01);
        this.put(index + 5, value.m11);
        this.put(index + 6, value.m21);
        this.put(index + 7, value.m31);
        this.put(index + 8, value.m02);
        this.put(index + 9, value.m12);
        this.put(index + 10, value.m22);
        this.put(index + 11, value.m32);
        this.put(index + 12, value.m03);
        this.put(index + 13, value.m13);
        this.put(index + 14, value.m23);
        this.put(index + 15, value.m33);
    }

    public int getInt(int index) {
        if (index >= this.length) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return this.data[index];
    }

    public float getFloat(int index) {
        if (index >= this.length) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return Float.intBitsToFloat(this.data[index]);
    }

    public void clear() {
        this.wrapper.clear();
        this.length = 0;
    }

    public void rewind() {
        this.wrapper.rewind();
    }

    public int position() {
        return this.wrapper.position();
    }

    public void position(int newPosition) {
        this.wrapper.position(newPosition);
    }

    public String toStringAsInts(int dimension) {
        if (this.data.length == 0)
            return "[]";

        if (dimension > 0) {
            String out = "[\n";
            for (int i = 0; i < this.data.length / dimension; i++) {
                out += "\t[" + this.data[dimension * i];
                for (int j = 1; j < dimension; j++) {
                    int index = dimension * i + j;
                    if (this.data.length > index)
                        out += ", " + this.data[index];
                }
                out += "]\n";
            }
            out += "]\n";
            return out;
        } else {
            String out = "[";
            for (int i = 0; i < this.data.length - 1; i++) {
                out += this.data[i] + ", ";
            }
            out += this.data[this.data.length - 1] + "]\n";
            return out;
        }
    }

    public String toStringAsFloats(int dimension) {
        if (this.data.length == 0)
            return "[]";

        if (dimension > 0) {
            String out = "[\n";
            for (int i = 0; i < this.data.length / dimension; i++) {
                out += "\t[" + Float.intBitsToFloat(this.data[dimension * i]);
                for (int j = 1; j < dimension; j++) {
                    int index = dimension * i + j;
                    if (this.data.length > index)
                        out += ", " + Float.intBitsToFloat(this.data[index]);
                }
                out += "]\n";
            }
            out += "]\n";
            return out;
        } else {
            String out = "[";
            for (int i = 0; i < this.data.length - 1; i++) {
                out += Float.intBitsToFloat(this.data[i]) + ", ";
            }
            out += Float.intBitsToFloat(this.data[this.data.length - 1]) + "]\n";
            return out;
        }
    }

    public void send(GL4 gl) {
        this.createGLBufferIfNeeded(gl);
        this.wrapper.rewind();
        gl.glNamedBufferData(this.id, this.length() * Integer.BYTES, this.wrapper, this.usage.toGlValue());
    }

    public void bind(GL4 gl, BufferTarget target) {
        this.createGLBufferIfNeeded(gl);
        gl.glBindBuffer(target.toGLValue(), this.id);
    }

    public void bind(GL4 gl, BufferTarget target, int index) {
        this.createGLBufferIfNeeded(gl);
        gl.glBindBufferBase(target.toGLValue(), index, this.id);
    }

    protected void delete(GL4 gl) {
        if (this.id != -1)
            gl.glDeleteBuffers(0, new int[] {this.id}, 0);
        this.id = -1;
    }
}
