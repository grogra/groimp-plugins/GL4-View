package me.mlata.gl4.volumes;

import java.util.Objects;

import de.grogra.imp3d.PolygonArray;
import me.mlata.gl4.Buffer;
import me.mlata.gl4.Primitive;
import me.mlata.gl4.VAOBuilder;

public class Polygons extends Volume {
    PolygonArray parray;
    
    public Polygons(PolygonArray parray) {
        this.parray = parray; 
    }
    
    public Polygons() {
        this.parray = new PolygonArray(); 
    }
   
    @Override
    public boolean equalsInner(Object other) {
        return this.parray.equals(((Polygons) other).parray); 
    }

    @Override
    public int hashCodeInner() {
        return Objects.hash("polygons", this.parray);
    }

    @Override
    protected void buildMesh(VAOBuilder builder) {
        builder.addVertices(this.parray.vertices.toArray());
        if (this.parray.normals != null && !this.parray.usePolygonNormals) {
            Buffer normals = builder.getNormalBuffer(this.parray.normals.size());
            for (int i = 0; i < this.parray.normals.size(); i++) {
                normals.put((2 * (float) this.parray.normals.get(i) + 1) / 255);
            }
        }
        if (this.parray.edgeCount == 3) {
            builder.addIndices(this.parray.polygons.toArray());
        } else {
            Buffer indices = builder.getIndexBuffer(this.parray.polygons.size / 2 * 3);
            int[] polys = this.parray.polygons.elements;
            for (int quad = 0; quad < this.parray.polygons.size; quad += 4) {
                indices.put(new int[] {
                    polys[quad], polys[quad + 1], polys[quad + 2],
                    polys[quad], polys[quad + 2], polys[quad + 3],
                });
            }
        }
        builder.setPrimitive(Primitive.Triangles);
        builder.setVisibleSides(this.parray.visibleSides);
    }
}
