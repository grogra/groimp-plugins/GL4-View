package me.mlata.gl4.volumes;

import java.util.Objects;

import com.jogamp.opengl.GL4;

import me.mlata.gl4.VAOBuilder;
import me.mlata.gl4.VertexArrayObject;

/**
 * A Volume is an object containing logic to lazily create a
 * {@link VertexArrayObject} when requested.
 */
public abstract class Volume {
    VertexArrayObject vao;
    boolean wireframe = false;

    public boolean equals(Object other) {
        return other != null
            && this.getClass().equals(other.getClass())
            && this.equalsInner(other)
            && this.wireframe == ((Volume) other).wireframe;
    }
   
    /**
     * Performs subclass-specific comparison.
     */
    public abstract boolean equalsInner(Object other);

    public int hashCode() {
        return Objects.hash(this.hashCodeInner(), this.wireframe);
    }

    public abstract int hashCodeInner();

    /**
     * Converts the volume to wireframe rendering.
     * @return Itself.
     */
    public Volume asWireframe() {
        this.wireframe = true;
        return this;
    }

    public void deleteVAO(GL4 gl) {
        if (this.vao != null)
            this.vao.delete(gl);
        this.vao = null;
    }

    public VertexArrayObject getMesh(GL4 gl) {
        if (this.vao == null) {
            VAOBuilder builder = new VAOBuilder();
            this.buildMesh(builder);
            builder.setWireframe(this.wireframe);
            this.vao = builder.finish(gl);
        }
        return this.vao;
    }

    /**
     * Creates a {@link VertexArrayObject VAO} using the given {@link VAOBuilder}.
     */
    protected abstract void buildMesh(VAOBuilder volume);
}
