package me.mlata.gl4.volumes;

import java.util.Objects;

import me.mlata.gl4.Buffer;
import me.mlata.gl4.VAOBuilder;

public class Frustum extends Volume {
    private static final int COUNT = 40;
    private final int hashCode;

    // top radius / bottom radius in per mille
    int ratio = 1000;

    public float getRatio() {
        return (float) this.ratio / 1000;
    }

    public Frustum() {
        this.hashCode = Objects.hash("frustum", ratio);
    }

    public Frustum(float bottomRadius, float topRadius) {
        this.ratio = (int) (topRadius / bottomRadius * 1000);
        this.hashCode = Objects.hash("frustum", ratio);
    }

    @Override
    public boolean equalsInner(Object other) {
        return this.ratio == ((Frustum) other).ratio;
    }

    @Override
    public int hashCodeInner() {
        return hashCode;
    }

    @Override
    public String toString() {
        return "Frustum(" + this.ratio + "%)";
    }
    
    public void buildMesh(VAOBuilder builder) {
        // center vertices + top and bottom + each vertex needed twice * 3 coords per vertex * around cylinder
        int vertexCount = 2 + 2 * 2 * COUNT;
        System.out.println(vertexCount);
        Buffer vertices = builder.getVertexBuffer(3 * vertexCount);
        vertices.put(new float[] {0, 0, 0, 0, 0, 1});
        float[] bottom = circle(1, COUNT, 0);
        float[] top = circle(1 * this.getRatio(), COUNT, 1);
        float[] sideNormals = circle(1, COUNT, 1 - this.getRatio(), true);
        System.out.println(bottom.length);
        System.out.println(top.length);
        // bottom
        vertices.put(bottom);
        // top
        vertices.put(top);
        // side
        vertices.put(bottom);
        vertices.put(top);

        // TODO: check normals, they look off
        Buffer normals = builder.getNormalBuffer(3 * vertexCount);
        // normals for top and bottom center points
        normals.put(new float[] {0, 0, -1, 0, 0, 1});

        // normals for top and bottom surfaces
        for (int i = 0; i < COUNT; i++) {
            normals.put(new float[] {0, 0, -1});
        }
        for (int i = 0; i < COUNT; i++) {
            normals.put(new float[] {0, 0, 1});
        }

        // surfaces
        for (int i = 0; i < COUNT; i++) {
            bottom[3*i+2] = 0;
            float scale = 1 / (float) Math.sqrt(
                bottom[3*i] * bottom[3*i]
                + bottom[3*((i+1)%COUNT)] * bottom[3*((i+1)%COUNT)]
            );
            bottom[3*i] *= scale;
            bottom[3*((i+1)%COUNT)] *= scale;

            top[3*i+2] = 0;
            scale = 1 / (float) Math.sqrt(
                top[3*i] * top[3*i]
                + top[3*((i+1)%COUNT)] * top[3*((i+1)%COUNT)]
            );
            top[3*i] *= scale;
            top[3*((i+1)%COUNT)] *= scale;
        }

        // normals for side surfaces
        normals.put(sideNormals);
        normals.put(sideNormals);

        Buffer indices = builder.getIndexBuffer(3 * 4 * COUNT);
        for (int i = 0; i < COUNT; i++) {
            indices.put(new int[] {
                // bottom
                0, 2+(i+1)%COUNT, 2+i,

                // top
                1, 2+COUNT+i, 2+COUNT+(i+1)%COUNT,

                // side
                2+2*COUNT+i, 2+2*COUNT+(i+1)%COUNT, 2+2*COUNT+COUNT+i,
                2+2*COUNT+(i+1)%COUNT, 2+2*COUNT+COUNT+(i+1)%COUNT, 2+2*COUNT+COUNT+i
            });
        }
    }

    public float[] circle(float radius, int vertexCount, float z) {
        return circle(radius, vertexCount, z, false);
    }

    public float[] circle(float radius, int vertexCount, float z, boolean normalize) {
        float[] out = new float[3 * vertexCount];
        for (int i = 0; i < vertexCount; i++) {
            float angle = 2 * (float) Math.PI * ((float) i) / (float) vertexCount;
            float x = (float) Math.cos(angle);
            float y = (float) Math.sin(angle);
            out[3*i] = radius * x;
            out[3*i+1] = radius * y;
            out[3*i+2] = z;
            if (normalize) {
                float length = (float) Math.sqrt(out[3*i] * out[3*i] + out[3*i+1] * out[3*i+1] + out[3*i+2] * out[3*i+2]);
                out[3*i] /= length;
                out[3*i+1] /= length;
                out[3*i+2] /= length;
            }
        }
        return out;
    }
}
