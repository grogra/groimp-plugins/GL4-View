package me.mlata.gl4;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.ImageObserver;
import java.awt.image.RenderedImage;
import java.io.IOException;
import java.util.Map.Entry;
import java.util.logging.Logger;

import javax.swing.JPanel;
import javax.vecmath.Matrix4d;
import javax.vecmath.Matrix4f;

import com.jogamp.opengl.DebugGL4;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLJPanel;

import de.grogra.imp.View;
import de.grogra.imp.awt.ViewComponentAdapter;
import de.grogra.imp3d.Camera;
import de.grogra.imp3d.View3D;
import de.grogra.imp3d.shading.Light;
import de.grogra.pf.boot.Main;
import de.grogra.pf.ui.Workbench;
import de.grogra.util.EventListener;
import de.grogra.util.Lock;
import de.grogra.util.LockProtectedRunnable;
import de.grogra.util.Utils;
import de.grogra.xl.lang.ObjectConsumer;
import me.mlata.gl4.volumes.Volume;

/**
 * Main display class representing the renderer.
 */
public class GLDisplay extends ViewComponentAdapter implements GLEventListener, ImageObserver {
    // Last graph stamp, used to detect whether the graph changed
    int oldStamp = Integer.MIN_VALUE;

    LightCollection lights;

    Program program;

    ObjectCollector collector;
    ObjectCollector toolsCollector;
    ObjectCollector selectedCollector;

    JPanel wrapper;
    GLAutoDrawable canvas;
    /**
     * Set to <code>true</code> when {@link #canvas} has changed. This indicates
     * that a new GLContext is used.
     */
    volatile boolean canvasChanged = false;
    /**
     * Set to <code>true</code> by {@link #reshape} in order to indicate that
     * the size of the canvas has changed.
     */
    volatile boolean reshaped = true;

    private Lock retainedLock = null;
    private boolean disableRetain = false;
    private final Object lockMutex = new Object ();
    private volatile int repaintFlags;

    final Logger logger = Main.getLogger ();

    final Object imageLock = new Object ();

    // ——— GLEventListener ———
    
    /**
     * Called by JOGL when the GLDrawable gets initialized. Initial setup of the renderer happens here.
     */
    @Override
    public void init(GLAutoDrawable d) {
        GL anyGl = d.getGL();

        // check if opengl4 is supported by device
        GL4 gl;
        if (anyGl instanceof GL4) {
            gl = (GL4) anyGl;
        } else {
            throw new RuntimeException("Device does not support OpenGL 4");
        }

        // enable depth testing
        gl.glEnable(GL.GL_DEPTH_TEST);

        // set clear color for frame buffer
        gl.glClearColor(0.75f, 0.75f, 0.75f, 0);

        // set opengl primitive restart index
        gl.glPrimitiveRestartIndex(-1);

        // set line params
        gl.glLineWidth(1);
        gl.glEnable(GL4.GL_LINE_SMOOTH);
        // set point size
        gl.glPointSize(10);

        // initialize ObjectCollector
        // TODO: shared vCache
        this.collector = new ObjectCollector(this);
		this.collector.setResolution(getDisplayResolution());
        this.toolsCollector = new ObjectCollector(this);
        this.selectedCollector = new ObjectCollector(this);

        // compile and link shaders
        try {
            byte[] v = getClass().getClassLoader()
                    .getResourceAsStream("shader/vertex.glsl").readAllBytes();
            byte[] f = getClass().getClassLoader()
                    .getResourceAsStream("shader/fragment.glsl").readAllBytes();
            program = new Program(gl, 
                v,
                f
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Called by JOGL to render a frame. This does not render a frame immediately, but instead blocks until GroIMP
     * requests the next frame to be rendered.
     */
    @Override
    public void display(GLAutoDrawable d) {
        Lock lock;
        synchronized (lockMutex) {
            lock = retainedLock;
            retainedLock = null;
            if (lock != null) {
                disableRetain = true;
            }
        }
        if (lock != null) {
            try {
                Workbench.setCurrent(getView ().getWorkbench ());
                Utils.executeForcedlyAndUninterruptibly (
                    getView().getGraph (), new LockProtectedRunnable() {
                        @Override
                        public void run(boolean sameThread, Lock lock) {
                            int flags;
                            synchronized (lockMutex) {
                                disableRetain = false;
                                flags = repaintFlags;
                                repaintFlags = 0;
                            }
                            invokeRenderSync(flags);
                        }
                    }, lock);
            } finally {
                Workbench.setCurrent(null);
                synchronized (lockMutex) {
                    disableRetain = false;
                }
            }
        } else {
            repaint(reshaped ? (ALL | CHANGED) : ALL);
        }
    }

    /**
     * Called by JOGL when the Drawable is disposed. 
     */
    @Override
    public void dispose(GLAutoDrawable d) {
        super.dispose();
        GL4 gl = (GL4) d.getGL();

        this.program.delete(gl);
        this.collector.delete(gl);
        this.toolsCollector.delete(gl);
        this.selectedCollector.delete(gl);
    }

    /**
     * Called by JOGL when the dimensions of the drawable change.
     */
    @Override
    public void reshape(GLAutoDrawable d, int x, int y, int width, int height) {
        
    }

    // ——— ViewComponentAdapter ———

    @Override
    public Object getComponent() {
        return wrapper;
    }

    public View3D getView3D() {
        return (View3D) getView ();
    }

    @Override
    protected void initRender(int flags) {}

    @Override
    protected void invokeRender(final int flags) {
        Utils.executeForcedlyAndUninterruptibly(getView().getGraph(),
            new LockProtectedRunnable() {
                @Override
                public void run(boolean sameThread, Lock lock) {
                    boolean display = false;
                    synchronized (lockMutex) {
                        repaintFlags |= flags;
                        if (!disableRetain) {
                            lock.retain ();
                            retainedLock = lock;
                            display = true;
                        }
                    }
                    if (display) {
                        canvas.display();
                    }
                }
            }, false);
    }

    @Override
    public void initView(View view, EventListener listener) {
        super.initView(view, listener);
        // TODO: check which options from the old renderer are useful and implement those again
        GLCapabilities glCaps = new GLCapabilities(GLProfile.get(GLProfile.GL4));
        canvas = new GLJPanel(glCaps) {
            @Override
            public void addNotify() {
                super.addNotify ();
                installListeners (wrapper);
            }
            
            @Override
            public void removeNotify() {
                super.removeNotify ();
                uninstallListeners(wrapper);
            }
        };
        wrapper = (GLJPanel) canvas;
        canvasChanged = true;
        canvas.addGLEventListener(GLDisplay.this);
        wrapper.setMinimumSize(new Dimension (0, 0));
        wrapper.setPreferredSize(new Dimension (640, 480));
    }

    /**
     * This is the method that actually performs the rendering at each frame.
     */
    @Override
    protected void render(int flags) throws InterruptedException {
        long startTime = System.nanoTime();
        
        // TODO: find out why this is here
        getView3D().setExtent(null, Float.NaN);

        // GL4 gl = (GL4) this.canvas.getGL();
        DebugGL4 gl = new DebugGL4((GL4) this.canvas.getGL());

        // TODO: check if the glContext changed

        // get viewport size
        int w = wrapper.getWidth();
        int h = wrapper.getHeight();

        this.getView3D()
            .getCanvasCamera()
            .setDimension(w, h);

        // TODO: return support for background images

        // clear OpenGL buffers
        gl.glClear(GL.GL_DEPTH_BUFFER_BIT | GL.GL_COLOR_BUFFER_BIT);

        // check if the scene graph changed
        int newStamp = getView().getGraph().getStamp();
        if (newStamp != oldStamp) {
            oldStamp = newStamp;
            collector.collect(gl);

            // TODO: wireframe rendering
            // TODO: highlight selected objects
            // TODO: draw tools
        }

        // collect tools
        toolsCollector.collectTools(gl);

        // collect selected objects
        selectedCollector.collectSelected(gl);

        // select OpenGL program
        program.use(gl);

        // get view and projection matrices from camera
        Matrix4f view = this.getViewMatrix();
        Matrix4f clip = this.getProjectionMatrix();

        // upload clip matrix
        gl.glUniformMatrix4fv(
            6,
            1,
            false,
            matrix4fToGLArray(clip),
            0
        );

        // calculate viewclip matrix
        clip.mul(view);

        // upload matrices to GPU
        gl.glUniformMatrix4fv(
            0,
            1,
            false,
            matrix4fToGLArray(view),
            0
        );
        gl.glUniformMatrix4fv(
            1,
            1,
            false,
            matrix4fToGLArray(clip),
            0
        );

        // update default light
        Matrix4d t = new Matrix4d();
        Light light = this.getView3D().getDefaultLight(t);
        collector.getLights().setDefaultLight(light, t);

        // bind light collection
        collector.getLights().bind(gl);

        // draw all object collections
        for (Entry<Volume, ObjectCollection> entry : collector.entrySet()) {
            entry.getValue().draw(gl);
        }

        // clear depth buffer to draw tools on top
        gl.glClear(GL.GL_DEPTH_BUFFER_BIT);

        // draw tools
        for (Entry<Volume, ObjectCollection> entry : toolsCollector.entrySet()) {
            entry.getValue().draw(gl, false, true);
        }

        // clear depth buffer to draw tools on top
        gl.glClear(GL.GL_DEPTH_BUFFER_BIT);

        // overdraw selected objects with wireframe
        for (Entry<Volume, ObjectCollection> entry : selectedCollector.entrySet()) {
            // TODO: this does not work for some reason
            // entry.getValue().draw(gl, true, true);
        }

        // execute all issued commands
        gl.glFlush();

        // print render time
        long endTime = System.nanoTime();
        System.err.println("Rendered in " + ((float) (endTime - startTime) / 1_000_000));
    }

    // ——— ImageObserver ———
    
    /**
     * This class implements the ImageObserver interface to be notified
     * about changes of the background image.
     */
    @Override
    public boolean imageUpdate (Image img, int infoflags, int x, int y, int width, int height) {
        if ((infoflags & (ABORT | ERROR)) != 0) {
            return false;
        }

        // TODO
        // new image data is available
        // so update the image and repaint the window

        // synchronized (imageLock) {
        // 	// remember the image
        // 	this.img = img;
        // 	imgChanged = true;

        // 	// activate redraw of window
        // 	repaint(RENDERED_IMAGE);
        // }

        return true;
    }

    private Matrix4f getViewMatrix() {
        return new Matrix4f(this.getView3D().getCamera().getWorldToViewTransformation());
    }

    private Matrix4f getProjectionMatrix() {
        float aspect = (float) this.wrapper.getWidth() / (float) this.wrapper.getHeight();
        Camera camera = this.getView3D().getCamera();
        Matrix4d projectiond = new Matrix4d();
        camera.getViewToClipTransformation(projectiond);
        Matrix4f projection = new Matrix4f(projectiond);
        projection.mul(
            new Matrix4f(
                1, 0, 0, 0,
                0, aspect, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
            )
        );
        return projection;
    }

    private float[] matrix4fToGLArray(Matrix4f m) {
        return new float[] {
            m.m00, m.m10, m.m20, m.m30,
            m.m01, m.m11, m.m21, m.m31,
            m.m02, m.m12, m.m22, m.m32,
            m.m03, m.m13, m.m23, m.m33
        };
    }

    @Override
    public void makeSnapshot (ObjectConsumer<? super RenderedImage> callback) {
        // TODO
    }

    @Override
    protected ImageObserver getObserverForRenderer() {
        // TODO
        return this;
    }
    
    @Override
    public void updateResolution(int r) {
    	this.collector.setResolution(r);
    }
}
