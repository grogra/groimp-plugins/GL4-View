package me.mlata.gl4;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL4;
import de.grogra.imp3d.objects.Attributes;

/**
 * This class represents an OpenGL VertexArrayObject (VAO). It is usually created by a Volume using a VAOBuilder
 * and interacting with it directly should not be required.
 */
public class VertexArrayObject {
    /**
     * A buffer containing vertex position data as a densely packed array of floats.
     */
    Buffer vertices;
    /**
     * A buffer containing vertex normal data as a densely packed array of floats.
     */
    Buffer normals;
    /**
     * A buffer containing indices as a densely packed array of ints.
     */
    Buffer indices;
    /**
     * The ID of the VAO.
     */
    int id;

    /**
     * The type of OpenGL primitive to use.
     */
    Primitive primitive;
    /**
     * Whether to use billboarding when rendering.
     */
    boolean billboarding;
    /**
     * Whether to calculate shading when rendering.
     */
    boolean shading;
    /**
     * Whether to draw only the edges of the object.
     */
    boolean wireframe;
    /**
     * Which sides of the triangles should be visible.
     */
    int visibleSides;

    /**
     * The OpenGL attribute index for position data.
     */
    static final int POSITION_ATTRIB_INDEX = 0;

    /**
     * The OpenGL attribute index for normal data.
     */
    static final int NORMAL_ATTRIB_INDEX = 1;

    /**
     * The uniform location for the billboarding uniform.
     */
    static final int BILLBOARDING_UNIFORM_LOCATION = 5;

    /**
     * Binds the VAO to the given OpenGL context and sets up required uniforms
     * and other OpenGL state.
     * @param gl The current OpenGL context
     */
    public void bind(GL4 gl) {
        // bind VAO
        gl.glBindVertexArray(this.id);

        // TODO: figure out why setting up the vao every frame is required
        vertices.bind(gl, BufferTarget.ARRAY);
        gl.glVertexAttribPointer(POSITION_ATTRIB_INDEX, 3, GL.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(POSITION_ATTRIB_INDEX);
        if (this.normals != null && this.shading) {
            normals.bind(gl, BufferTarget.ARRAY);
            gl.glVertexAttribPointer(NORMAL_ATTRIB_INDEX, 3, GL.GL_FLOAT, false, 0, 0);
            gl.glEnableVertexAttribArray(NORMAL_ATTRIB_INDEX);
        }

        // bind indices
        if (this.indices != null) {
            indices.bind(gl, BufferTarget.ELEMENT_ARRAY);
        }

        // set uniforms
        gl.glUniform1i(BILLBOARDING_UNIFORM_LOCATION, billboarding ? 1 : 0);

        // set visible sides
        switch (this.visibleSides) {
            case Attributes.VISIBLE_SIDES_FRONT:
                // enable back face culling
                gl.glEnable(GL4.GL_CULL_FACE);
                gl.glFrontFace(GL4.GL_CCW);
                break;
            case Attributes.VISIBLE_SIDES_BACK:
                // enable front face culling
                gl.glEnable(GL4.GL_CULL_FACE);
                gl.glFrontFace(GL4.GL_CW);
                break;
            case Attributes.VISIBLE_SIDES_BOTH:
                gl.glDisable(GL4.GL_CULL_FACE);
                break;
        }
    }

    public void setWireframe(boolean wireframe) {
        this.wireframe = wireframe;
    }

    /**
     * @return The OpenGL constant belonging to the primitive type.
     */
    protected int getGLPrimitive() {
        return this.primitive.toGLValue();
    }

    /**
     * @return The amount of indices.
     */
    protected int getIndicesCount() {
        return this.indices.length();
    }

    /**
     * @return The amount of vertex coordinates.
     */
    protected int getVerticesCount() {
        return this.vertices.length();
    }

    /**
     * @return Whether this vao contains an index buffer.
     */
    protected boolean hasIndices() {
        return this.indices != null;
    }

    /**
     * The constructor for a VAO. It is recommended to use a VAOBuilder instead.
     */
    protected VertexArrayObject(
        GL4 gl,
        Buffer vertices,
        Buffer normals,
        Buffer indices,
        Primitive primitive,
        boolean wireframe,
        boolean shading,
        boolean billboarding,
        int visibleSides
    ) {
        // create a OpenGL VAO
        int[] ids = new int[1];
        gl.glGenVertexArrays(1, ids, 0);
        this.id = ids[0];

        // bind vao
        gl.glBindVertexArray(this.id);
        
        // setup vertices
        vertices.send(gl);
        vertices.bind(gl, BufferTarget.ARRAY);
        gl.glVertexAttribPointer(POSITION_ATTRIB_INDEX, 3, GL.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(POSITION_ATTRIB_INDEX);

        // setup normals
        if (normals != null) {
            normals.send(gl);
            normals.bind(gl, BufferTarget.ARRAY);
            gl.glVertexAttribPointer(NORMAL_ATTRIB_INDEX, 3, GL.GL_FLOAT, false, 0, 0);
            gl.glEnableVertexAttribArray(NORMAL_ATTRIB_INDEX);
        }

        // setup indices
        if (indices != null) {
            indices.send(gl);
        }

        // save buffers
        this.vertices = vertices;
        this.normals = normals;
        this.indices = indices;

        // set attributes
        this.primitive = primitive;
        this.wireframe = wireframe;
        this.shading = shading;
        this.billboarding = billboarding;
        this.visibleSides = visibleSides;
    }

    public String toString() {
        String out = "VAO: \n- vertices: ";
        out += this.vertices.length();
        if (this.indices != null) {
            out += "- indices: " + this.indices.length() / 3 + "\n";
        }
        out += "- primitive: " + this.primitive + "\n";
        out += "- billboarding: " + this.billboarding + "\n";
        out += "- shading: " + this.shading + "\n";
        out += "- wireframe: " + this.wireframe + "\n";

        return out;
    }

    /**
     * Deletes the VAO from the OpenGL context and frees up its memory.
     * @param gl The current OpenGL context.
     */
    public void delete(GL4 gl) {
        // Delete OpenGL buffers when this gets cleaned up
        gl.glDeleteVertexArrays(1, new int[]{this.id}, 0);
    }
}
