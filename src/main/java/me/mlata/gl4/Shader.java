package me.mlata.gl4;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.jogamp.opengl.GL4;

/**
 * A glsl shader.
 */
public class Shader {
    int id;
    
    public Shader(GL4 gl, String src, ShaderKind kind) {
        this.id = gl.glCreateShader(kind.toGLValue());
        gl.glShaderSource(this.id, 1, new String[]{src}, new int[]{src.length()}, 0);
        gl.glCompileShader(this.id);

        int[] success = new int[] {1};
        gl.glGetShaderiv(this.id, GL4.GL_COMPILE_STATUS, success, 0);
        if (success[0] == 0) {
            int[] len = new int[] {0};
            gl.glGetShaderiv(this.id, GL4.GL_INFO_LOG_LENGTH, len, 0);
            byte[] infoLogBytes = new byte[len[0]];
            gl.glGetShaderInfoLog(this.id, len[0], len, 0, infoLogBytes, 0);
            String infoLog = new String(infoLogBytes);
            throw new RuntimeException("Shader compilation failed (" + kind + "): " + infoLog + "\nShader:\n" + src);
        }
    }

    public Shader(GL4 gl, Path src, ShaderKind kind) throws IOException {
        this(
            gl,
            new String(Files.readAllBytes(src)),
            kind
        );
    }

    public Shader(GL4 gl, File src, ShaderKind kind) throws IOException {
        this(
            gl,
            Paths.get(src.getPath()),
            kind
        );
    }

    public void delete(GL4 gl) {
        gl.glDeleteShader(this.id);
    }
}
