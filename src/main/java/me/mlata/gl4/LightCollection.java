package me.mlata.gl4;

import javax.vecmath.Color3f;
import javax.vecmath.Matrix4d;
import javax.vecmath.Matrix4f;
import javax.vecmath.Vector4f;

import com.jogamp.opengl.GL4;

import de.grogra.imp3d.objects.DirectionalLight;
import de.grogra.imp3d.objects.LightBase;
import de.grogra.imp3d.objects.PointLight;
import de.grogra.imp3d.objects.SpotLight;
import de.grogra.imp3d.shading.Light;

// TODO: check why point light power below 0.013 result in white sphere

/**
 * A OpenGL buffer storing lights.
 * The lights are stored the following way:
 * 
 * Transforms buffer (8 bytes per light):
 * - 4 bytes position
 * - 4 bytes orientation
 *
 * Data buffer (8 bytes per light):
 * - 1 byte kind
 * - 1 byte power (total for point/spot, per m^2 for directional)
 * - 1 byte inner angle (spot)
 * - 1 byte outer angle (spot)
 * - 3 bytes color
 * - 1 byte gap 
 * 
 * 32 bytes are used in total per light.
 * Due due limitations to the maximum size of uniform buffers, only 512 lights are supported.
 * When more lights are added to the scene, only the first 512 are used in rendering.
 * The selection of light chosen in this case should not be relied upon.
 */
public class LightCollection {
    int index = 0;

    Buffer transforms;
    Buffer data;

    boolean isDefaultLight;
    Light defaultLight;
    Matrix4f defaultLightTransform;

    static final int POINT_LIGHT = 0;
    static final int SPOT_LIGHT = 1;
    static final int DIRECTIONAL_LIGHT = 2;

    static final int LIGHT_TRANSFORM_BINDING = 4;
    static final int LIGHT_DATA_BINDING = 5;

    static final int LIGHT_COUNT_UNIFORM_LOCATION = 2;
    static final int REINHARD_LUMINOSITY_UNIFORM_lOCATION = 3;

    public static final int MAX_LIGHT_COUNT = Buffer.MAX_UNIFORM_BUFFER_SIZE / 8;

    static final float PI = 3.1415926535897932384626433832795f;

    int count = 0;
    int totalPower = 0;

    /**
     * Creates a new {@link LightCollection}.
     * @param gl The current OpenGL context
     */
    public LightCollection(GL4 gl) {
        this.transforms = new Buffer(Buffer.MAX_UNIFORM_BUFFER_SIZE, BufferUsage.DYNAMIC_DRAW);
        this.data = new Buffer(Buffer.MAX_UNIFORM_BUFFER_SIZE, BufferUsage.DYNAMIC_DRAW);
    }

    /**
     * Adds a light to the collection.
     * @param light The light to be collected
     * @param t The transform matrix of the light in world space
     */
    public void put(Light light, Matrix4f t) {
        // check if max amount of lights reached
        if (this.count == MAX_LIGHT_COUNT) {
            return;
        }

        // declare and init light params
        float power = 0;
        Color3f color = new Color3f(1, 1, 1);
        int kind = -1;
        float innerAngle = 0;
        float outerAngle = 0;
        
        // check light type and set params accordingly
        if (light instanceof LightBase) {
            LightBase lightBase = (LightBase) light;
            color = lightBase.getColor();

            if (lightBase instanceof PointLight) {
                PointLight pointLight = (PointLight) lightBase;
                kind = POINT_LIGHT;

                power = pointLight.getPower();

                // update total light power
                this.totalPower += power / 4 / PI;

                if (lightBase instanceof SpotLight) {
                    SpotLight spotLight = (SpotLight) lightBase;
                    kind = SPOT_LIGHT;

                    innerAngle = spotLight.getInnerAngle();
                    outerAngle = spotLight.getOuterAngle();
                }
            }

            if (lightBase instanceof DirectionalLight) {
                DirectionalLight directionalLight = (DirectionalLight) lightBase;
                kind = DIRECTIONAL_LIGHT;

                power = directionalLight.getPowerDensity();

                // update total light power
                this.totalPower += power;
            }
        }

        if (kind == -1) {
            return;
        }

        // increment light count
        this.count++;

        // get position and orientation vector from transfrom matrix
        Vector4f position = new Vector4f(0, 0, 0, 1);
        Vector4f orientation = new Vector4f(0, 0, 1, 0);
        t.transform(position);
        t.transform(orientation);

        // add light data to buffers
        this.transforms.put(position);
        this.transforms.put(orientation);

        this.data.put(kind);
        this.data.put(power);
        this.data.put(innerAngle);
        this.data.put(outerAngle);
        this.data.put(color);
        this.data.put(0);
    }

    /**
     * Returns the number of lights collected.
     */
    public int getLightCount() {
        return this.count;
    }

    /**
     * Sets the default light to use if no other lights got collected.
     */
    public void setDefaultLight(Light light, Matrix4d transform) {
        this.defaultLight = light;
        this.defaultLightTransform = new Matrix4f(transform);
    }

    /**
     * Sends the collected light data to the GPU.
     * @param gl The current OpenGL context
     */
    protected void send(GL4 gl) {
        this.transforms.send(gl);
        this.data.send(gl);
    }

    /**
     * Binds the OpenGL buffers and sets required uniforms.
     * This also sends the default light to the GPU in case no lights got
     * collected. 
     * @param gl The current OpenGL context
     */
    protected void bind(GL4 gl) {
        // handle default light
        if (this.isDefaultLight || this.getLightCount() == 0) {
            this.clear();
            this.put(this.defaultLight, this.defaultLightTransform);
            this.send(gl);
            this.isDefaultLight = true;
        }

        // bind buffers
        this.transforms.bind(gl, BufferTarget.UNIFORM, LIGHT_TRANSFORM_BINDING);
        this.data.bind(gl, BufferTarget.UNIFORM, LIGHT_DATA_BINDING);

        // set shaders
        gl.glUniform1i(LIGHT_COUNT_UNIFORM_LOCATION, this.count);
        gl.glUniform1f(REINHARD_LUMINOSITY_UNIFORM_lOCATION, this.totalPower);
    }

    /**
     * Removes all lights from this collection.
     */
    protected void clear() {
        // clear buffers
        this.transforms.clear();
        this.data.clear();

        // reset variables
        this.count = 0;
        this.totalPower = 0;
        this.isDefaultLight = false;
    }
}
