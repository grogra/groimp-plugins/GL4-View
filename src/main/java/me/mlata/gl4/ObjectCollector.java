package me.mlata.gl4;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.util.HashMap;
import java.util.Set;
import java.util.Map.Entry;

import javax.vecmath.Color3f;
import javax.vecmath.Matrix4d;
import javax.vecmath.Matrix4f;
import javax.vecmath.Tuple2f;
import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3f;

import com.jogamp.opengl.GL4;

import de.grogra.graph.ArrayPath;
import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.Graph;
import de.grogra.graph.GraphState;
import de.grogra.graph.GraphUtils;
import de.grogra.graph.Path;
import de.grogra.graph.impl.GraphManager;
import de.grogra.imp.edit.ViewSelection;
import de.grogra.imp3d.CanvasCamera;
import de.grogra.imp3d.DisplayVisitor;
import de.grogra.imp3d.LineSegmentizable;
import de.grogra.imp3d.Polygonizable;
import de.grogra.imp3d.Polygonization;
import de.grogra.imp3d.RenderState;
import de.grogra.imp3d.Renderable;
import de.grogra.imp3d.View3D;
import de.grogra.imp3d.objects.LightNode;
import de.grogra.imp3d.shading.Shader;
import de.grogra.math.Pool;
import de.grogra.vecmath.Math2;
import me.mlata.gl4.volumes.*;

/**
 * This class collects objects from the GroIMP scene and sorts them into
 * {@link ObjectCollection ObjectCollectinos} by {@link Volume}. Multiple methods are
 * available for object collection depending on what should be collected.
 */
public class ObjectCollector implements RenderState {
    GLDisplay display;

    // cache of volumes generated from LineSegmentizable and Polygonizable objects
    VolumeCache vCache;
    HashMap<Volume, ObjectCollection> objects = new HashMap<Volume, ObjectCollection>();
    LightCollection lights;
    GLVisitor visitor = new GLVisitor();

    // gets set to identity matrix in constructor
    final Matrix4d identity;

    // gets set every frame to give draw* methods access to opengl context
    GL4 gl = null;

    public ObjectCollector(GLDisplay display) {
        identity = new Matrix4d();
        identity.setIdentity();
        this.display = display;
        this.vCache = new VolumeCache(this.getRenderGraphState(), Polygonization.COMPUTE_NORMALS | Polygonization.COMPUTE_UV, 1, true);
    }

    /**
     * Collects all objects in the scene.
     */
    public void collect(GL4 gl) {
        this.gl = gl;
        if (this.lights == null) {
            this.lights = new LightCollection(gl);
        }

        // clear lights
        this.lights.clear();
        
        // clear buffers
        for (Entry<Volume, ObjectCollection> collection : objects.entrySet()) {
            collection.getValue().clear();
        }

        // visit and collect all objects
        visitor.init(getRenderGraphState(),
            getView3D().getCamera().getWorldToViewTransformation(), true);
        getView3D().getGraph().accept(null, visitor, null);

        // send collected objects to GPU
        this.lights.send(gl);
        for (Entry<Volume, ObjectCollection> collection : objects.entrySet()) {
            collection.getValue().send(gl);
        }
    }

    /**
     * Collects all objects in the tools graph.
     */
    public void collectTools(GL4 gl) {
        this.gl = gl;
        
        if (this.lights == null) {
            this.lights = new LightCollection(gl);
        }

        // clear lights
        this.lights.clear();
        
        // clear buffers
        for (Entry<Volume, ObjectCollection> collection : objects.entrySet()) {
            collection.getValue().clear();
        }

        // visit and collect tools
        de.grogra.imp.edit.Tool tool = getView3D().getActiveTool ();
        if (tool != null) {
            curHighlight = 0;
            visitor.init(
                GraphManager.STATIC_STATE,
                getView3D().getCamera().getWorldToViewTransformation(),
                false
            );
            ArrayPath path = new ArrayPath(getView3D().getGraph());
            path.clear(GraphManager.STATIC);
            for (int i = 0; i < tool.getToolCount (); i++) {
                GraphManager.acceptGraph (tool.getRoot (i), visitor, path);
            }
        }

        // send collected objects to GPU
        this.lights.send(gl);
        for (Entry<Volume, ObjectCollection> collection : objects.entrySet()) {
            collection.getValue().send(gl);
        }
    }

    /**
     * Collects all selected objects in the scene.
     */
    public void collectSelected(GL4 gl) {
        this.gl = gl;
        
        if (this.lights == null) {
            this.lights = new LightCollection(gl);
        }

        // clear lights
        this.lights.clear();
        
        // clear buffers
        for (Entry<Volume, ObjectCollection> collection : objects.entrySet()) {
            collection.getValue().clear();
        }

        ArrayPath path = new ArrayPath(getView3D().getGraph());
        ViewSelection.Entry[] s = ViewSelection.get(getView3D()).getAll(-1);
        for (int i = 0; i < s.length; i++) {
            Path p = s[i].getPath();
            curHighlight = s[i].getValue ();
            visitor.init(
                getRenderGraphState(),
                getView3D().getCamera().getWorldToViewTransformation(),
                true
            );
            GraphUtils.acceptPath(p, visitor, path);
        }


        // send collected objects to GPU
        this.lights.send(gl);
        for (Entry<Volume, ObjectCollection> collection : objects.entrySet()) {
            collection.getValue().send(gl);
        }
    }
    
    //TODO: test & remove
    private void collectVolume(Volume type, Volume volume, Matrix4f transform) {
        ObjectCollection collection = this.objects.get(type);
        // create new collection if none exists for the object type
        if (collection == null) {
            collection = new ObjectCollection(type);
            this.objects.put(volume, collection);
        }
        collection.put(gl, transform, this.visitor.currentColor);
    }

    private void collectVolume(Volume volume, Matrix4f transform) {
        ObjectCollection collection = this.objects.get(volume);
        // create new collection if none exists for the object type
        if (collection == null) {
            collection = new ObjectCollection(volume);
            this.objects.put(volume, collection);
        }
        collection.put(gl, transform, this.visitor.currentColor);
    }

    private void collectVolume(Volume volume, Matrix4f transform, Tuple3f color) {
        ObjectCollection collection = this.objects.get(volume);
        // create new collection if none exists for the object type
        if (collection == null) {
            collection = new ObjectCollection(volume);
            this.objects.put(volume, collection);
        }
        collection.put(gl, transform, color);
        
    }

    public ObjectCollection get(Volume volume) {
        return this.objects.get(volume);
    }

    public Set<Entry<Volume, ObjectCollection>> entrySet() {
        return this.objects.entrySet();
    }

    public LightCollection getLights() {
        return this.lights;
    }

    /**
     * This class is used to traverse the graph and obtain all
     * visible objects and the lights for visualisation.
     * 
     * @author Manuel Lata
     */
    private class GLVisitor extends DisplayVisitor {
        Color3f currentColor;

        Matrix4d worldToViewInv = new Matrix4d();

//        void init(GraphState gs, Matrix4d t, boolean checkLayer) {
//            init(gs, t, getView3D(), checkLayer);
//
//            Matrix4d worldToView = getView3D().getCamera().getWorldToViewTransformation();
//            worldToViewInv.invert(worldToView);
//        }
        void init(GraphState gs, Matrix4d t, boolean checkLayer) {
            init(gs,new EdgePatternImpl(
					Graph.BRANCH_EDGE | Graph.SUCCESSOR_EDGE, Graph.IGNORED_EDGE, true, true),
            		t, getView3D(), checkLayer);

            Matrix4d worldToView = getView3D().getCamera().getWorldToViewTransformation();
            worldToViewInv.invert(worldToView);
        }

        @Override
        protected void visitImpl(Object object, boolean asNode, Shader s, Path path) {
            // query shape
            Object shape = state.getObjectDefault(object, asNode,
                de.grogra.imp3d.objects.Attributes.SHAPE, null);
            // query color
            Color3f color = (Color3f) state.getObjectDefault(object, asNode,
                de.grogra.imp3d.objects.Attributes.COLOR, new Color3f(new Color(s.getAverageColor())));

            this.currentColor = color;

            // check if there is a shape object
            if (shape != null) {
                // collect lights
                if (shape instanceof LightNode) {
                    lights.put(((LightNode) shape).getLight(), new Matrix4f(ObjectCollector.this.getTransform(null)));
                }

                // draw objects like sphere, box, cone
                if (shape instanceof Renderable) {
                    ((Renderable) shape).draw(object, asNode, ObjectCollector.this);
                    return;
                }

                // draw polygonizable objects that are not renderable for some reason
                if (shape instanceof Polygonizable) {
                    Volume volume = vCache.get(object, asNode, (Polygonizable) shape);
                    collectVolume(volume, getTransform(null), color);
                }

                // draw linesegmentizable objects
                if (shape instanceof LineSegmentizable) {
                    Volume volume = vCache.get(object, asNode, (LineSegmentizable) shape);
                    collectVolume(volume, getTransform(null), color);
                }
            }
        }
    }

    public View3D getView3D() {
        return display.getView3D();
    }

    protected void delete(GL4 gl) {
        for (Entry<Volume, ObjectCollection> collection : objects.entrySet()) {
            collection.getValue().delete(gl);
        }
    }

    protected Matrix4f getTransform(Matrix4d t) {
        // return new Matrix4f(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);

        Matrix4d tnew;
        if (t == null) {
            tnew = new Matrix4d(visitor.getCurrentTransformation());
        } else {
            tnew = new Matrix4d();
            Math2.mulAffine(tnew, visitor.getCurrentTransformation(), t);
        }
        tnew.mul(this.visitor.worldToViewInv, tnew);
        return new Matrix4f(tnew);
    }

    private final Volume POINT = new Point();
    public void drawPoint (Tuple3f location, int pixelSize, Tuple3f color, int highlight, Matrix4d t) {
        Matrix4f ts = this.getTransform(t);
        Matrix4f tnew = new Matrix4f(
            1, 0, 0, location.x,
            0, 1, 0, location.y,
            0, 0, 1, location.z,
            0, 0, 0, 1
        );
        ts.mul(tnew);

        this.collectVolume(POINT, ts, color);
    }
    
    /**
     * Draw a set of points. The array locations contains a sequence of points given as triples
     * of floats for x, y and z position of each point. If the same reference for the array
     * location is passed in the implementation might assume that the contents of the array
     * are the same as well. This is necessary for GLDisplay, for instance, to ensure a 
     * performant implementation. The class PointCloud ensures that this is the case.
     * @param locations array containing a sequence (x,y,z) of points
     * @param pointSize size of the point on screen
     * @param color color of the point
     * @param highlight
     * @param t transformation of the point cloud
     */
    public void drawPointCloud (float[] locations, float pointSize, Tuple3f color, int highlight, Matrix4d t) {
        for (int i = 0; i < locations.length; i += 3) {
            this.drawPoint(new Vector3f(locations[i], locations[i + 1], locations[i + 2]), (int) pointSize, color, highlight, t);
        }
    }

    private final Volume LINE = new Lines();
    public void drawLine (Tuple3f start, Tuple3f end, Tuple3f color, int highlight, Matrix4d t) {
        Matrix4f ts = getTransform(t);
        
        Matrix4f shift = new Matrix4f(
            1, 0, 0, start.x,
            0, 1, 0, start.y,
            0, 0, 1, start.z,
            0, 0, 0, 1
        );
        Matrix4f rot = new Matrix4f(
            end.x - start.x, 0, 0, 0,
            end.y - start.y, 0, 0, 0,
            end.z - start.z, 0, 0, 0,
            0, 0, 0, 1
        );

        ts.mul(shift);
        ts.mul(rot);

        this.collectVolume(LINE, ts, color);
    }

    public void drawParallelogram (float axis, Vector3f secondAxis, float scaleU, float scaleV, Shader s, int highlight, boolean asWireframe, Matrix4d t) {
        
    }

    public void drawPlane (Shader s, int highlight, boolean asWireframe, Matrix4d t) {
        
    }

    private final Volume SPHERE = new Sphere();
    private final Volume SPHERE_WF = new Sphere().asWireframe();
    public void drawSphere (float radius, Shader s, int highlight, boolean asWireframe, Matrix4d t) {
        Matrix4f ts = getTransform(t);

        // Scale sphere by radius
        Matrix4f r = new Matrix4f();
        r.set(new Vector3f(), radius);
        ts.mul(r);

        this.collectVolume(asWireframe ? SPHERE_WF : SPHERE, ts);
    }

    public void drawTextBlock(String caption, Font font, float depth, Shader s, int highlight, boolean asWireframe, Matrix4d t) {
        
    }

    /**
     * Draw a supershape around the origin (0/0/0).
     * 
     * An implementation of Johan Gielis's Superformula which was published in the
     * American Journal of Botany 90(3): 333–338. 2003.
     * INVITED SPECIAL PAPER A GENERIC GEOMETRIC TRANSFORMATION 
     * THAT UNIFIES A WIDE RANGE OF NATURAL AND ABSTRACT SHAPES
     *
     * @param a, b length of curves 
     * @param m, n shape parameters
     * @param shader
     * @param highlight
     * @param t transformation of the point cloud
     */
    public void drawSupershape (float a, float b, float m1, float n11, float n12, float n13, float m2, float n21, float n22, float n23, Shader s, int highlight, boolean asWireframe, Matrix4d t) {
        
    }
 
    private final Volume CUBE = new Cube();
    private final Volume CUBE_WF = new Cube().asWireframe();
    /**
     * 
     * @param halfWidth
     * @param halfLength
     * @param height
     * @param s
     * @param highlight
     * @param t
     */
    public void drawBox (float halfWidth, float halfLength, float height, Shader s, int highlight, boolean asWireframe, Matrix4d t) {
        Matrix4f ts = getTransform(t);

        // scale box to desired size
        Matrix4f r = new Matrix4f(
            halfWidth * 2, 0, 0, 0,
            0, halfLength * 2, 0, 0,
            0, 0, height, 0,
            0, 0, 0, 1
        );
        ts.mul(r);

        this.collectVolume(asWireframe ? CUBE_WF : CUBE, ts);
    }

    /**
     * 
     * @param halfWidth
     * @param halfLength
     * @param height
     * @param a amplitude
     * @param b frequency
     * @param s
     * @param highlight
     * @param t
     */
    public void drawLamella (float halfWidth, float halfLength, float height, float a, float b, Shader s, int highlight, boolean asWireframe, Matrix4d t) {
        
    }

    /**
     * 
     * @param height
     * @param baseRadius
     * @param topRadius
     * @param baseClosed
     * @param topClosed
     * @param scaleV
     * @param s
     * @param highlight
     * @param t
     */
    public void drawFrustum (float height, float baseRadius, float topRadius, boolean baseClosed, boolean topClosed, float scaleV, Shader s, int highlight, boolean asWireframe, Matrix4d t) {
        Matrix4f ts = getTransform(t);

        // scale frustum to desired size
        Matrix4f r = new Matrix4f(
            baseRadius, 0, 0, 0,
            0, baseRadius, 0, 0,
            0, 0, height, 0,
            0, 0, 0, 1
        );
        ts.mul(r);

        this.collectVolume(new Frustum(baseRadius, topRadius), ts);
    }

    private final Volume POLYGONS = new Polygons();
    /**
     * 
     * @param polygons
     * @param obj
     * @param asNode
     * @param s
     * @param highlight
     * @param t
     */
    public void drawPolygons(Polygonizable polygons, Object obj, boolean asNode, Shader s, int highlight, boolean asWireframe, Matrix4d t) {
        Matrix4f ts = getTransform(t);
        Volume volume = vCache.get(obj, asNode, (Polygonizable) polygons);
        collectVolume(volume, getTransform(null));
        //collectVolume(POLYGONS, volume, getTransform(null));
    }

    /**
     * 
     * @param y
     * @param xPos
     * @param xNeg
     * @param zPos
     * @param zNeg
     * @param highlight
     * @param t
     */
    public void drawPrismRectangular(float y, float xPos, float xNeg, float zPos, float zNeg, int highlight,boolean asWireframe,  Matrix4d t) {
        
    }
    
    /**
     * Computes the window coordinates in pixels of a location
     * in the current object coordinates.
     * 
     * @param location a location in local object coordinates
     * @param out the computed window coordinates are placed in here
     * @return <code>true</code> iff the window coordinates are valid
     * (i.e., the location is in the clipping region)
     */
    public boolean getWindowPos (Tuple3f origin, Tuple2f out) {
        Matrix4d m = visitor.getCurrentTransformation ();
        return getView3D()
            .getCanvasCamera()
            .projectView(
                (float) (origin.x * m.m00 + origin.y * m.m01 + origin.z * m.m02 + m.m03),
                (float) (origin.x * m.m10 + origin.y * m.m11 + origin.z * m.m12 + m.m13),
                (float) (origin.x * m.m20 + origin.y * m.m21 + origin.z * m.m22 + m.m23),
                out, true) == CanvasCamera.INSIDE_CLIPPING;
    }

    

    public void drawRectangle (int x, int y, int w, int h, Tuple3f color) {
        
    }
    
    public void fillRectangle (int x, int y, int w, int h, Tuple3f color) {
        
    }

    public void drawString (int x, int y, String text, Font font, Tuple3f color) {
        
    }
    
    public void drawFrustumIrregular(float height, int sectorCount, float[] baseRadii, float[] topRadii,  boolean baseClosed, boolean topclosed,  float scaleV, Shader s, int highlight, boolean asWireframe, Matrix4d t) {
        
    }

    public void drawSphereSegmentSolid(float radius, float theta1, float theta2, float phi, Shader s, int highlight, boolean asWireframe, Matrix4d t) {
        
    }

    
    public final Pool pool = new Pool ();
    @Override
    public Pool getPool ()
    {
        return pool;
    }

    @Override
    public Shader getCurrentShader ()
    {
        return visitor.getCurrentShader ();
    }

    private int curHighlight = 0;

    @Override
    public int getCurrentHighlight ()
    {
        return curHighlight;
    }
    
    public float estimateScaleAt (Tuple3f point) {
        // TODO
        return 1;
    }


    Graphics graphics;
    @Override
    public FontMetrics getFontMetrics (Font font)
    {
        return graphics.getFontMetrics (font);
    }

    @Override
    public GraphState getRenderGraphState() {
        return display.getRenderGraphState();
    }
    
    public void setResolution(int r) {
    	this.visitor.setResolution(r);
    }
}
