package me.mlata.gl4;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Tuple3f;
import javax.vecmath.Tuple4f;

import com.jogamp.opengl.GL4;

/**
 * A convenience wrapper using multiple buffers in case the required buffer size
 * exceeds the maximum buffer size. Writing to the buffer works the same way as writing
 * to a regular {@link Buffer}, but individual buffers have to be bound using
 * {@link bindNext} when rendering.
 */
public class ChunkedBuffer {
    List<Buffer> objects;
    int chunkSize;
    int current = 0;

    int bound = -1;

    public ChunkedBuffer(int chunkSize) {
        this.chunkSize = chunkSize;
        // Initialize objects with single chunk, more will be added on demand
        this.objects = new ArrayList<Buffer>(1);
        this.objects.add(new Buffer(0, BufferUsage.DYNAMIC_DRAW));
    }

    private Buffer getNextWithCapacity(int requiredSpace) {
        if (this.getCurrent().remaining() >= requiredSpace) {
            return this.getCurrent();
        } else if (this.chunkSize - this.getCurrent().length() >= requiredSpace) {
            this.getCurrent().resize(this.getCurrent().length() + requiredSpace);            
            return this.getCurrent();
        } else {
            this.current++;
            if (this.current==1800) {
            	System.out.println("test");
            }
            if (this.current%100==0) {
            	System.out.println("test");
            }
            if (this.current == this.objects.size()) {
                this.objects.add(new Buffer(requiredSpace, BufferUsage.DYNAMIC_DRAW));
            }
            return this.getCurrent();
        }
    }

    protected boolean bindNext(GL4 gl, BufferTarget target) {
        this.bound++;
        if (this.bound >= this.objects.size()) {
            return false; 
        } else {
            objects.get(this.bound).bind(gl, target);
            return true;
        }
    }

    protected boolean bindNext(GL4 gl, BufferTarget target, int index) {
        this.bound++;
        if (this.bound >= this.objects.size()) {
            return false; 
        } else {
            objects.get(this.bound).bind(gl, target, index);
            return true;
        }
    }

    protected void resetBound(GL4 gl) {
        this.bound = -1;
    }

    public Buffer getBoundBuffer() {
        if (this.bound >= this.objects.size()) {
            return null;
        } else {
            return this.objects.get(this.bound);
        }
    }

    public int getTotalSize() {
        int sum = 0;
        for (Buffer buf : this.objects) {
            sum += buf.length();
        }
        return sum;
    }

    public Collection<Buffer> buffers() {
        return this.objects;
    }

    protected Buffer getCurrent() {
        return this.objects.get(this.current);
    }

    public void put(int value) {
        this.getNextWithCapacity( 1).put(value);
    }

    public void put(int index, int value) {
        this.getCurrent().put(index, value);
    }

    public void put(int[] values) {
        this.getNextWithCapacity(values.length).put(values);
    }

    public void put(float value) {
        this.getNextWithCapacity(1).put(value);
    }

    public void put(int index, float value) {
        this.getCurrent().put(index, value);
    }

    public void put(float[] values) {
        this.getNextWithCapacity(values.length).put(values);
    }

    public void put(Tuple3f value) {
        this.getNextWithCapacity(3).put(value);
    }

    public void put(int index, Tuple3f value) {
        this.getCurrent().put(index, value);
    }

    public void put(Tuple4f value) {
        this.getNextWithCapacity(4).put(value);
    }

    public void put(int index, Tuple4f value) {
        this.getCurrent().put(index, value);
    }

    public void write(Matrix3f value) {
        this.getNextWithCapacity(9).put(value);
    }

    public void write(int index, Matrix3f value) {
        this.getCurrent().put(index, value);
    }

    public void put(Matrix4f value) {
        this.getNextWithCapacity(16).put(value);
    }

    public void put(int index, Matrix4f value) {
        this.getCurrent().put(index, value);
    }

    public void clear() {
        for (Buffer buf : this.objects) {
            buf.clear();
        }
        this.current = 0;
    }

    public void send(GL4 gl) {
        for (Buffer buf : this.objects) {
            buf.send(gl);
        }
    }

    protected void delete(GL4 gl) {
        for (Buffer buf : this.objects) {
            buf.delete(gl);
        }
    }
}
