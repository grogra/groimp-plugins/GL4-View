package me.mlata.gl4;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.GLProfile;

@TestMethodOrder(OrderAnnotation.class)
@Order(0)
public class GLTest {
    public static GL4 gl;

    @Test
    @Order(0)
    public void glInit() {
        final GLProfile glp = GLProfile.get(GLProfile.GL2ES2);
        assertNotNull(glp);
        final GLCapabilities caps = new GLCapabilities(glp);
        assertNotNull(caps);

        final GLWindow window = GLWindow.create(caps);
        assertNotNull(window);
        window.setSize(800, 600);
        window.setVisible(true);
        assertTrue(window.isNativeValid());

        final GLContext context = window.getContext();

        // trigger native creation of drawable/context
        window.display();
        assertTrue(window.isRealized());
        assertTrue(window.getContext().isCreated());

        context.makeCurrent();
        GLTest.gl = (GL4) window.getGL();
    }

    public boolean testGL() {
        return true;
    }
}
