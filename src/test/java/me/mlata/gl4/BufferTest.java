package me.mlata.gl4;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import javax.vecmath.Matrix4f;

import org.junit.jupiter.api.Test;

/**
 * Class to test the functionality of GLBuffer
 */
public class BufferTest {
    @Test
    public void createBuffer() {
        Buffer buffer = new Buffer(16, BufferUsage.STATIC_DRAW);
    }

    @Test
    public void sendBuffer() {
        Buffer buffer = new Buffer(16, BufferUsage.STATIC_DRAW);
        buffer.put(new Matrix4f());
        buffer.send(GLTest.gl);
    }

    @Test
    public void clearBuffer() {
        Buffer buffer = new Buffer(16, BufferUsage.STATIC_DRAW);
        assertEquals(0, buffer.length());
        buffer.put(new Matrix4f());
        assertEquals(16, buffer.length());
        buffer.clear();
        assertEquals(0, buffer.length());
    }

    @Test
    public void resizeBuffer() {
        Buffer buffer = new Buffer( 4, BufferUsage.STATIC_DRAW);
        buffer.put(new int[]{ 1, 2, 3, 4 });
        buffer.resize(8);
        for (int i = 0; i < 4; i++) {
            assertEquals(buffer.getInt(i), i + 1);
        }
        assertEquals(4, buffer.position());
        try {
            buffer.getInt(4);
            fail("Buffer contained additional values");
        } catch (ArrayIndexOutOfBoundsException e) {
            
        }
        buffer.resize(2);
        for (int i = 0; i < 2; i++) {
            assertEquals(buffer.getInt(i), i + 1);
        }
        assertEquals(2, buffer.position());
    }
}
