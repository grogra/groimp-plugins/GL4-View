package me.mlata.gl4;

import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.Test;

public class ShaderTest {
    @Test
    public void loadShader() {
        ClassLoader loader = this.getClass().getClassLoader();
        assertNotNull(new File(loader.getResource("shader/vertex.glsl").getFile()));
        assertNotNull(new File(loader.getResource("shader/fragment.glsl").getFile()));
    }

    @Test
    public void compileShader() throws IOException {
        ClassLoader loader = this.getClass().getClassLoader();
        File vertex = new File(loader.getResource("shader/vertex.glsl").getFile());
        File fragment = new File(loader.getResource("shader/fragment.glsl").getFile());
        
        assertNotNull(new Shader(GLTest.gl, vertex, ShaderKind.Vertex));
        assertNotNull(new Shader(GLTest.gl, fragment, ShaderKind.Fragment));
    }

    @Test
    public void linkProgram() throws IOException {
        ClassLoader loader = this.getClass().getClassLoader();
        File vertex = new File(loader.getResource("shader/vertex.glsl").getFile());
        File fragment = new File(loader.getResource("shader/fragment.glsl").getFile());
        
        assertNotNull(new Program(GLTest.gl,
            new Shader(GLTest.gl, vertex, ShaderKind.Vertex),
            new Shader(GLTest.gl, fragment, ShaderKind.Fragment)
        ));
    }
}
